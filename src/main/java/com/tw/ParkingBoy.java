package com.tw;

public class ParkingBoy {
    private final ParkingLot parkingLot;
    private String lastErrorMessage;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingTicket park(Car car) {
        // TODO: Implement the method according to test
        // <-start-
        ParkingResult parkingInfo = parkingLot.park(car);
        if (!parkingInfo.isSuccess()) {
            lastErrorMessage = parkingInfo.getMessage();
        }
        return parkingLot.park(car).getTicket();
        // ---end->
    }

    public Car fetch(ParkingTicket ticket) {
        // TODO: Implement the method according to test
        // ]<-start-
        FetchingResult fetchedInfo = parkingLot.fetch(ticket);
        if(fetchedInfo.getCar() == null) {
            lastErrorMessage = fetchedInfo.getMessage();
        }
        return fetchedInfo.getCar();
        // ---end->
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
}
